// JSON Objects
/*
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

/*
	{
		"city": "Mumbai",
		"state": "Maharashtra",
		"country": "India"
	}
*/

/*
	JSON Arrays

	"cities": [
		{"city": "Mumbai", "state": "Maharashtra", "country": "India"}, {"city": "Quezon city", "province": "Metro Manila",
		"country": "Philipines"}
	]
*/

// JSON Methods

// Converting Data into Stringified JSON

let batchesArr = [{ batchName: 'Batch X' },{ batchName: 'Batch Y' }];

console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'Onkar',
	age: 29,
	address: {
		city: 'Mumbai',
		country: 'India'
	}
});

console.log(data);

// Using stringify method with variables
/*
	Syntax:
		JSON.stringify({
			propertyA: variableA,
			propertyB: variableB
		})
*/

// User details

let firstName = prompt('what is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]`;

console.log('Result from parse method;');
console.log(JSON.parse(batchesJSON));

let stringifiedObject = '{ "name": "Aarush", "age": "2", "address": { "city": "Pune", "country": "India" } }';

console.log(JSON.parse(stringifiedObject));